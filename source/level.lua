import 'rabbit'
import 'carrot'
import 'carrot_field'
import 'carrot_crumbs'
import 'ui'

local pd <const> = playdate
local gfx <const> = pd.graphics

local STAGE_IDLE <const> = "idle"
local STAGE_PLAYING <const> = "playing"
local STAGE_OVER <const> = "gameover"

local playTime = 30 * 1000 -- 30 seconds per round

local rabbit = Rabbit()
local carrot = Carrot()
local carrotField = CarrotField()
local ui = UI()

class("Level").extends(gfx.sprite)

function Level:init()
    Level.super.init(self)

    math.randomseed(pd.getSecondsSinceEpoch())
    self:setZIndex(0)

    self.gameStage = STAGE_IDLE
    self.score = 0
    self.playTimer = nil

    -- add sprites to the scene
    rabbit:addSprite()
    carrot:addSprite()
    carrotField:addSprite()
    self:addSprite()

    ui:setStage(STAGE_IDLE)
    ui:addSprite()

    self:resetTimer()
    self:drawBackground()
end

function Level:resetTimer()
    self.playTimer = pd.timer.new(playTime, playTime, 0, pd.easingFunctions.linear)
end

function Level:drawBackground()
    local backgroundImage = gfx.image.new("images/bg")
    gfx.sprite.setBackgroundDrawingCallback(
        function(x, y, width, height)
            gfx.setClipRect(x, y, width, height)
            backgroundImage:draw(0, 0)
            gfx.clearClipRect()
        end
    )
end

function Level:moveCarrot(x, y)
    local posX = x or math.random(40, 360)
    local posY = y or math.random(40, 200)
    carrot:moveTo(posX, posY)
    carrotField:moveTo(posX, posY)
end

function Level:showCarrotCrumbs(x, y)
    local carrotCrumbs = CarrotCrumbs()
    carrotCrumbs:moveTo(x, y)
    carrotCrumbs:add()
end

function Level:startGame()
    self.gameStage = STAGE_PLAYING
    ui:setStage(self.gameStage)
    self:resetTimer()
    self:moveCarrot()
end

function Level:finishGame()
    self.gameStage = STAGE_OVER
    ui:setStage(self.gameStage)
    rabbit:freeze()
end

function Level:resetGame()
    self.gameStage = STAGE_IDLE
    ui:setStage(self.gameStage)
    self:moveCarrot(999, 999)
    rabbit:reset()
    self.score = 0
    ui:setScore(self.score)
end

function Level:checkNear()
    local carrotFieldCollisions = carrotField:overlappingSprites()
    if #carrotFieldCollisions == 2 then
        rabbit:eat()
    else
        rabbit:unEat()
    end
end

function Level:checkScore()
    local carrotCollisions = carrot:overlappingSprites()
    if #carrotCollisions == 2 then
        rabbit:eatDone()
        self:showCarrotCrumbs(carrot:getPosition())
        self:moveCarrot()
        self.score += 1
        ui:setScore(self.score)
    end
end

function Level:update()
    if self.gameStage == STAGE_PLAYING then
        self:checkNear()
        self:checkScore()
        pd.timer.updateTimers()

        ui:setTime(self.playTimer.value)
        if self.playTimer.value == 0 then
            self:finishGame()
        end
    elseif self.gameStage == STAGE_OVER then
        if pd.buttonJustPressed(pd.kButtonA) then
            self:resetGame()
        end
    else
        if pd.buttonJustPressed(pd.kButtonUp) or pd.buttonJustPressed(pd.kButtonRight) or
            pd.buttonJustPressed(pd.kButtonDown) or pd.buttonIsPressed(pd.kButtonLeft) then
            self:startGame()
        end
    end
end