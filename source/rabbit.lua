local pd <const> = playdate
local gfx <const> = pd.graphics

class("Rabbit").extends(gfx.sprite)

local playerSpeed = 4
local imageIndex = 1

local animateWaitDuration <const> = 4
local animateWait = animateWaitDuration
-- animate step forward and backward
local ANIMATE_STEP_F, ANIMATE_STEP_B = 1, -1

local idleFrames = {1, 16}
local eatFrames = {17, 20}
local eatDoneFrames = {21, 23}

local LEFT, RIGHT = 1, 2
local facing = LEFT

local NORMAL, EATING, EAT_DONE, UNEAT = 1, 2, 3, 4
local state = NORMAL
local isDemo = true
local canMove = true

function Rabbit:init()
    Rabbit.super.init(self)

    self.images = gfx.imagetable.new("images/rabbit")
    self:setImage(self.images:getImage(idleFrames[1]))
    self:setZIndex(1000)
    self:moveTo(70, 120)
    w, h = self.images:getImage(idleFrames[1]):getSize()
    self:setCollideRect(0, 16, w, h - 16)
end

function Rabbit:freeze()
    canMove = false
end

function Rabbit:reset()
    self:moveTo(70, 120)
    state = NORMAL
    imageIndex = 1
    facing = LEFT
    canMove = true
    isDemo = true
end

function Rabbit:update()
    if canMove then
        if pd.buttonIsPressed(pd.kButtonUp) then
            isDemo = false
            self:moveBy(0, -playerSpeed)
        end
        if pd.buttonIsPressed(pd.kButtonRight) then
            isDemo = false
            facing = RIGHT
            self:moveBy(playerSpeed, 0)
        end
        if pd.buttonIsPressed(pd.kButtonDown) then
            isDemo = false
            self:moveBy(0, playerSpeed)
        end
        if pd.buttonIsPressed(pd.kButtonLeft) then
            isDemo = false
            facing = LEFT
            self:moveBy(-playerSpeed, 0)
        end
        if isDemo and pd.buttonJustPressed(pd.kButtonA) then
            if state ~= EATING and state ~= EAT_DONE then
                self:eat()
            end
        end
    end

    self:updateImage()
end

function Rabbit:updateImage()
    if facing == RIGHT then
        self:setImage(self.images:getImage(imageIndex), "flipX")
    else
        self:setImage(self.images:getImage(imageIndex))
    end

    if state == EATING or state == EAT_DONE or state == UNEAT then
        animateWait -= 2
    else
        animateWait -= 1
    end
    
    if animateWait <= 0 then
        animateWait = animateWaitDuration
        if state == UNEAT then
            imageIndex += ANIMATE_STEP_B
        else
            imageIndex += ANIMATE_STEP_F
        end

        if state == NORMAL then
            if imageIndex > idleFrames[2] then
                imageIndex = 1
            end
        elseif state == EATING then
            if imageIndex > eatFrames[2] then
                if isDemo then
                    -- when demo, auto "eat done"
                    self:eatDone()
                else
                    -- when playing, hold the frame
                    imageIndex = eatFrames[2]
                end
            end
        elseif state == EAT_DONE then
            if imageIndex > eatDoneFrames[2] then
                state = NORMAL
                imageIndex = 1
            end
        elseif state == UNEAT then
            if imageIndex < eatFrames[1] then
                state = NORMAL
                imageIndex = 1
            end
        end
    end
end

function Rabbit:eat()
    if state ~= EATING then
        state = EATING
        imageIndex = eatFrames[1]
    end
end

function Rabbit:unEat()
    if state == EATING then
        state = UNEAT
    end
end

function Rabbit:eatDone()
    if state ~= EAT_DONE then
        state = EAT_DONE
        imageIndex = eatDoneFrames[1]
    end
end