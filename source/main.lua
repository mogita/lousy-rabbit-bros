import "CoreLibs/object"
import "CoreLibs/graphics"
import "CoreLibs/sprites"
import "CoreLibs/timer"

import 'level'

local pd <const> = playdate
local gfx <const> = pd.graphics

playdate.display.setRefreshRate(30)

local level = Level()

function playdate.update()
	gfx.sprite.update()
end
