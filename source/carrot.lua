local pd <const> = playdate
local gfx <const> = pd.graphics

class("Carrot").extends(gfx.sprite)

function Carrot:init()
    Carrot.super.init(self)

	self.image = gfx.image.new("images/carrot")
    self:setImage(self.image)
    self:setZIndex(900)
    self:moveTo(999, 999)
    w, h = self.image:getSize()
	self:setCollideRect(3, 6, w - 5, h - 10)
end

