local pd <const> = playdate
local gfx <const> = pd.graphics

class("CarrotCrumbs").extends(gfx.sprite)

local animateWaitDuration <const> = 1
local animateWait = animateWaitDuration
local imageIndex = 1

function CarrotCrumbs:init()
    CarrotCrumbs.super.init(self)

    imageIndex = 1
    self.images = gfx.imagetable.new("images/carrot_crumbs")
    self:setImage(self.images:getImage(1))
    self:setZIndex(1000)
end

function CarrotCrumbs:update()
    if imageIndex > 10 then
        print(imageIndex)
    end
    self:setImage(self.images:getImage(imageIndex))

    animateWait -= 1
    if animateWait <= 0 then
        animateWait = animateWaitDuration
        imageIndex += 1
        if imageIndex > 10 then
            imageIndex = 10
            self:remove()
        end
    end
end