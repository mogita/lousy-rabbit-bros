local pd <const> = playdate
local gfx <const> = pd.graphics
-- for using the built-in font
local defaultGfx <const> = pd.graphics

class("UI").extends(gfx.sprite)

local STAGE_IDLE <const> = "idle"
local STAGE_PLAYING <const> = "playing"
local STAGE_OVER <const> = "gameover"

function UI:init()
    UI.super.init(self)

    self.font = gfx.font.new('font/PixelatedPusab')
    gfx.setFont(self.font)
    self.stage = ""
    self.score = 0
    self.time = 0

    self:setZIndex(1999)
    self:setIgnoresDrawOffset(true)
    self:setCenter(0, 0)
    -- important for the text to actually appear
    self:setSize(pd.display.getSize())
end

function UI:setStage(stage)
    -- self:clearClipRect()
    self.stage = stage
    self:markDirty()
end

function UI:setScore(score)
    self.score = score
    self:markDirty()
end

function UI:setTime(time)
    self.time = time
    self:markDirty()
end

function UI:draw()
    if self.stage == STAGE_IDLE then
        self:drawDemoScreen()
    elseif self.stage == STAGE_PLAYING then
        self:drawPlayingScreen()
    elseif self.stage == STAGE_OVER then
        self:drawGameOverScreen()
    end
end

function UI:drawDemoScreen()
    gfx.drawText("LOUSY RABBIT BROS", 130, 95)
    gfx.drawText("by @mogita", 130, 125)
    gfx.drawText("press any of                     to start", 55, 210)
    defaultGfx.drawText("⬆️ ➡️ ⬇️ ⬅️", 177, 207)
end

function UI:drawPlayingScreen()
    gfx.drawText("Time: " .. math.ceil(self.time / 1000), 8, 8)
    gfx.drawText("Score: " .. self.score, 305, 8)
end

function UI:drawGameOverScreen()
    gfx.setColor(gfx.kColorBlack)
    gfx.setLineWidth(5)
    gfx.drawRoundRect(145, 100, 120, 40, 6)
    gfx.setColor(gfx.kColorWhite)
    gfx.fillRoundRect(145, 100, 120, 40, 6)
    gfx.drawText("Score: " .. self.score, 167, 113)

    gfx.setColor(gfx.kColorBlack)
    gfx.setLineWidth(5)
    gfx.drawRoundRect(45, 186, 320, 26, 6)
    gfx.setColor(gfx.kColorWhite)
    gfx.fillRoundRect(45, 186, 320, 26, 6)
    gfx.drawText("press      key to play again", 92, 192)
    defaultGfx.drawText("Ⓐ", 147, 190)
end
