local pd <const> = playdate
local gfx <const> = pd.graphics

-- this class works as an extra collision box to the outter area of the arrot
-- it tells the rabbit to open the mouth
class("CarrotField").extends(gfx.sprite)

function CarrotField:init()
    CarrotField.super.init(self)

	self.image = gfx.image.new(128, 128, gfx.kColorClear)
    self:setImage(self.image)
    self:setZIndex(800)
	self:moveTo(999, 999)
	self:setCollideRect(0, 0, self.image:getSize())
end
