# Lousy Rabbit Bros

A hello-world-level game made for exploring [Playdate](https://play.date).

Sprites made with [Aseprite](https://www.aseprite.org).

Font `Pixelated Pusab` from [dafont.com](https://www.dafont.com/pixelatedpusab.font).

![Demo Video](lousy-rabbit-bros-demo.mp4)

# License

Code licensed under MIT © [mogita](https://gitlab.com/mogita), all media licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/). See the [LICENSE](LICENSE) file for details.
